package com.example.testcase;

import org.junit.Before;

import android.app.Activity;
import android.app.Instrumentation.ActivityMonitor;
import android.test.ActivityInstrumentationTestCase2;
import android.test.TouchUtils;
import android.view.KeyEvent;

import com.example.activities.TripPlanningActivity;
import com.google.android.gms.maps.GoogleMap;


public class PlanTripActivityTestCase extends ActivityInstrumentationTestCase2 <TripPlanningActivity>{
	
	TripPlanningActivity tripPlanningActivity;

	public PlanTripActivityTestCase() {
		super(TripPlanningActivity.class);
	}
	
	@Before
	protected void setUp() throws Exception {
		tripPlanningActivity = getActivity();
	};

	@org.junit.Test
	public void testMenuRemoveMarkerFromEmptyList() {
		ActivityMonitor actitivityMonitor = getInstrumentation().addMonitor(TripPlanningActivity.class.getName(), null, false);

		// Click the menu option
		getInstrumentation().sendKeyDownUpSync(KeyEvent.KEYCODE_MENU);
		getInstrumentation().invokeMenuActionSync(tripPlanningActivity, com.example.R.id.action_remove_tripPoint, 0);

		assertEquals("did not perform any action because there is no trip point in the list to remove", 0, tripPlanningActivity.pointsNo);
	}

}
