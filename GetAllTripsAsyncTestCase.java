package com.example.testcase;

import junit.framework.TestCase;

import org.junit.Test;

import com.example.rest.services.GetTripsAsync;

public class GetAllTripsAsyncTestCase extends TestCase {

	@Test
	public void testEmptyURL() {
		new GetTripsAsync() {
			protected void onPostExecute(java.util.List<com.example.classes.Trip> result) {
				assertTrue("no checking is done for empty URL", result == null);
			};
		}.execute("");
	}
	
}
