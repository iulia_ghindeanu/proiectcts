package com.example.testcase;

import org.junit.Before;

import android.app.Instrumentation.ActivityMonitor;
import android.test.ActivityInstrumentationTestCase2;
import android.view.KeyEvent;

import com.example.R;
import com.example.activities.AllTripsActivity;

public class AllTripsTestCase2 extends ActivityInstrumentationTestCase2<AllTripsActivity>{

	AllTripsActivity activity;
	
	public AllTripsTestCase2() {
		super(AllTripsActivity.class);
		// TODO Auto-generated constructor stub
	}
	
	@Before
	protected void setUp() throws Exception {
		activity = getActivity();
	};

	@org.junit.Test
	public void testContextMenu() {
		ActivityMonitor actitivityMonitor = getInstrumentation().addMonitor(AllTripsActivity.class.getName(), null, false);

		int initial = activity.allTrips.size();
		// Click the menu option
		getInstrumentation().sendKeyDownUpSync(KeyEvent.KEYCODE_MENU);
		//getInstrumentation().invokeMenuActionSync(allTripsActivity, R.id.ac, 0);
		getInstrumentation().invokeContextMenuAction(activity, R.string.action_contextmenu_deletetrip, 0);

		assertEquals("trip deleted", initial, activity.allTrips.size());
	}

}
