package com.example.testcase;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.BeforeClass;

import android.content.Intent;
import android.util.Log;
import android.widget.ListView;

import com.example.activities.AllTripsActivity;
import com.example.classes.Trip;
import com.example.util.Utils;

public class AllTripsTestCase extends
		android.test.ActivityUnitTestCase<AllTripsActivity> {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		Log.v("AllTripsAdapterTestCase", "setUpBeforeClass()"); // not showing
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		Log.v("AllTripsAdapterTestCase", "tearDownAfterClass()"); // not showing
	}

	AllTripsActivity allTripsActivity;

	public AllTripsTestCase() {
		super(AllTripsActivity.class);
	}

	// not working
	/*
	 * public static Test suite() { TestSetup setup = new TestSetup(new
	 * TestSuite(AllTripsAdapterTestCase.class)) { protected void setUp() throws
	 * TripGenericException { setUpBeforeClass(); }
	 * 
	 * protected void tearDown() { tearDownAfterClass(); } };
	 * 
	 * return setup; }
	 */

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		Log.v("AllTripsAdapterTestCase", "setUp()");

		Intent intent = new Intent(getInstrumentation().getTargetContext(),
				AllTripsActivity.class);
		startActivity(intent, null, null);
		allTripsActivity = getActivity();
	}

	@After
	public void tearDown() {
		Log.v("AllTripsAdapterTestCase", "tearDown()");
	}

	// error on Mockito
	/*
	 * @org.junit.Test public void testTripsAdapter() { //start the main
	 * activity of the application under test //on resume should be called and
	 * as a fact the call of async task GetTripsAsync mockTrips =
	 * Mockito.mock(GetTripsAsync.class); String url = Utils.URL_ALL_TRIPS;
	 * allTripsActivity.finish();
	 * Mockito.when(mockTrips.execute(url)).thenAnswer(new Answer<List<Trip>>()
	 * {
	 * 
	 * @Override public List<Trip> answer(InvocationOnMock invocation) throws
	 * Throwable { List<Trip> trips = new ArrayList<Trip>(); Trip t = new
	 * Trip("trip1"); t.setColor("#000000"); t.setZoom(15f);
	 * t.setTotalDistanceInMeters(500f); t.setTotalTimeInMiliseconds(420000l);
	 * Log.v("AllTripsAdapterTestCase", "setUp()"); return trips; } });
	 * allTripsActivity = getActivity();
	 * 
	 * }
	 */

	@org.junit.Test
	public void testNullListView() {
		final ListView lv = allTripsActivity.getListView();
		assertNotNull("ListView not allowed to be null", lv);
	}
	
	@org.junit.Test
	public void testItemClick() {
		int c = allTripsActivity.getListView().getAdapter().getCount();
		assertTrue(c != 0);
		boolean check = allTripsActivity.getListView().performItemClick(
				allTripsActivity.getListView().getAdapter().getView(0, null, null), 0, allTripsActivity.getListView().getAdapter()
						.getItemId(0));
		assertTrue("item clicked", check);

	}

	public void testIntentTriggerViaOnClick() {
		final ListView lv = allTripsActivity.getListView();
		lv.performItemClick(lv.getAdapter().getView(0, null, null), 0, lv.getAdapter().getItemId(0));

		// TouchUtils cannot be used, only allowed in
		// InstrumentationTestCase or ActivityInstrumentationTestCase2

		// Check the intent which was started
		Intent triggeredIntent = getStartedActivityIntent();
		assertNotNull("Intent was null", triggeredIntent);
	}
	
	public void testBundleViaOnClick() {
		final ListView lv = allTripsActivity.getListView();
		lv.performItemClick(lv.getAdapter().getView(0, null, null), 0, lv.getAdapter().getItemId(0));

		Intent triggeredIntent = getStartedActivityIntent();
		Trip data = (Trip) triggeredIntent.getExtras().getSerializable("trip");
		String url = triggeredIntent.getExtras().getString("url");

		assertEquals("Incorrect url passed via the intent",
				Utils.URL_ALL_TRIPS, url);

		assertFalse("Incorrect object passed via the intent", data == null);
	}

}
