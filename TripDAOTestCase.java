package com.example.testcase;

import junit.framework.TestCase;

import org.junit.Before;
import org.junit.Test;

import android.content.Context;

import com.example.activities.MyApplication;
import com.example.database.TripDao;

public class TripDAOTestCase extends TestCase {
	
	Context context;
	
	@Before
	protected void setUp (){
		context = MyApplication.getAppContext();
	}
	
	@Test
	public void testIsSingleton() {
		TripDao instance1 = TripDao.getInstance(MyApplication.getAppContext());
		TripDao instance2 = TripDao.getInstance(MyApplication.getAppContext());
		assertEquals(true, instance1 == instance2);
	}
	
	@Test
	public void testinsertTripCheckNull() {
		TripDao tripDao = TripDao.getInstance(context);
		assertEquals(tripDao.insert(null), null);
	}
	
	@Test
	public void testSelectByIdNegative() {
		TripDao tripDao = TripDao.getInstance(context);
		//assertEquals(tripDao.insert(null), null);
		System.out.println(tripDao.selectById(-1l));
	}
	
	@Test
	public void testSelectByIdNull() {
		TripDao tripDao = TripDao.getInstance(context);
		System.out.println(tripDao.selectById(null));
	}
	
	@Test
	public void testSelectByIdAberant() {
		TripDao tripDao = TripDao.getInstance(context);
		System.out.println(tripDao.selectById(Long.MAX_VALUE));
	}

}
