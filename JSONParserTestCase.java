package com.example.testcase;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.URL;

import junit.framework.TestCase;

import org.junit.Test;

import android.util.Log;

import com.example.classes.Trip;
import com.example.exceptions.JSONGenericException;
import com.example.util.JSONParser;
import com.example.util.Utils;

public class JSONParserTestCase extends TestCase{
	
	@Test
	public void testNullInputStream() {
		JSONParser parser = new JSONParser();
		try {
			parser.parseJSONArrayOfTrip(null);
			fail("nu arunca exceptie empty input stream for processing");
		} catch (JSONGenericException e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void testAccesLaRetea() {
		try {
			//Proxy proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress("cache.ase.ro", 8080));
            //HttpURLConnection conn = (HttpURLConnection) new URL (Utils.URL_ALL_TRIPS).openConnection(proxy);
			HttpURLConnection conn = (HttpURLConnection) new URL (Utils.URL_ALL_TRIPS).openConnection();
            String charset = "UTF-8";
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Accept-Charset", charset);
            
            conn.connect();
		} catch (Exception exc) {
        	exc.printStackTrace();
        	fail("cannot connect to the network");
        }
        
	}
	
	@Test
	public void testAutomatization() {
			// ========== TEST GET ALL TRIPS SERVICE ====================
			try {
				Proxy proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress("cache.ase.ro", 8080));
	            HttpURLConnection conn = (HttpURLConnection) new URL (Utils.URL_GET_TRIP + "/3").openConnection(proxy);
	            String charset = "UTF-8";
	            conn.setRequestMethod("GET");
	            conn.setRequestProperty("Accept-Charset", charset);
	            
	            conn.connect();
	            
	            InputStream response = conn.getInputStream();
	            JSONParser parser = new JSONParser();
	            
	            Trip trip = parser.parseJSONTrip(response);
	            
	            if (trip != null) {
	            	Log.v("TEST JSON PARSER", trip.toString());
				} else {
					fail("eroare la parsare");
				}
	            
			} catch (Exception exc) {
	        	exc.printStackTrace();
	        }
			
		}

}
