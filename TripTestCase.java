package com.example.testcase;

import junit.framework.TestCase;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import android.util.Log;

import com.example.classes.Trip;
import com.example.exceptions.TripGenericException;

public class TripTestCase extends TestCase {

	Trip tripForTest;

	@Before
	public void setUp() throws TripGenericException {
		System.out.println("setUp()");
		tripForTest = new Trip("test");
		tripForTest.setColor("#000000");
		tripForTest.setZoom(15f);
	}

	@After
	public void tearDown() {
		System.out.println("tearDown");
	}
	
	@Test
	public void testSetColorNull() {
		try {
			tripForTest.setColor(null);

			fail("Nu arunca exceptie null");
		} catch (TripGenericException e) {
			e.printStackTrace();
		}
	}

	@Test
	public void testSetColorVid() {
		try {
			tripForTest.setColor("");
			fail("Nu arunca exceptie sir vid");
		} catch (TripGenericException e) {
			e.printStackTrace();
		}
	}

	@Test
	public void testAddNullTripPoint() {
		boolean condition = tripForTest.addPoint(null);
		assertFalse(condition);
	}
	
	@Test
	public void testSetDistanceNegative() {
		try {
			tripForTest.setTotalDistanceInMeters(-1f);
			fail("exception not thrown");
		} catch (TripGenericException exc) {
			System.out.println(exc.getMessage());
		}
	}
	
	@Test
	public void testSetDistanceNull() throws TripGenericException {
		try {
			tripForTest.setTotalDistanceInMeters(null);
			fail("exception not thrown");
		} catch (TripGenericException exc) {
			System.out.println(exc.getMessage());
		}
	}
	
	@Test
	public void testSetTimeNegative() throws TripGenericException {
		try {
			tripForTest.setTotalTimeInMiliseconds(-100l);
			fail("exception not thrown");
		} catch (TripGenericException exc) {
			System.out.println(exc.getMessage());
		}
	}
	
	@Test
	public void testSetTimeNull() throws TripGenericException {
		try {
			tripForTest.setTotalTimeInMiliseconds(null);
			fail("exception not thrown");
		} catch (TripGenericException exc) {
			System.out.println(exc.getMessage());
		}
	}

	@Test
	public void testGetAvgSpeed() throws TripGenericException {
		tripForTest.setTotalDistanceInMeters(1000f);
		tripForTest.setTotalTimeInMiliseconds(690000l);

		double actual = tripForTest.getAvgSpeedKMperHour();
		double expected = 5.26;
		double delta = 0.1;
		assertEquals(expected, actual, delta);
	}


}
