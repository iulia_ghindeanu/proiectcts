package com.example.testcase;

import org.junit.Test;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.os.Handler;
import android.test.ActivityInstrumentationTestCase2;
import android.test.TouchUtils;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.example.R;
import com.example.activities.TripControlFacadeMainActivity;

public class TripControlMainActivityTestCase extends ActivityInstrumentationTestCase2<TripControlFacadeMainActivity>{

	private TripControlFacadeMainActivity tripMainActivity;
	
	//creare constructor default si trimitere activitate ca parametru
	public TripControlMainActivityTestCase() {
		super(TripControlFacadeMainActivity.class);
	}
	
	@Override
	protected void setUp() throws Exception {
		super.setUp();
		//pentru testare UI in metodele acestui testcase
		setActivityInitialTouchMode(true);
		//initializare activitate
		tripMainActivity = getActivity();
	}
	
	@Test
	public void testPreconditii() {
		assertNotNull("tripMainActivity e null", tripMainActivity);
	}
	
	@Test
	public void testNetworkEnabled() {
		ConnectivityManager manager = (ConnectivityManager) tripMainActivity.getSystemService(Context.CONNECTIVITY_SERVICE);
		assertEquals(manager.getActiveNetworkInfo().isConnected(), tripMainActivity.isConnected());
	}
	
	//testare click pe start tracking, avand acces la retea si location services pe device
	@Test
	public void testClickPowerButtonStartTrackingOtherConditionsAcomplished() {
		ImageButton powerButton = (ImageButton) tripMainActivity.findViewById(R.id.btnStartTrip);
		//pt simulare click pe buton
		TouchUtils.clickView(this, powerButton);
		SharedPreferences shared = tripMainActivity.getSharedPreferences("com.example.prefs", Context.MODE_PRIVATE);
		//nu poate incepe Trip nou pana nu ii seteaza numele si intervalul de notificare
		assertEquals(false, shared.getBoolean("tracking", false));
	}
	
	//testare stare "isTracking" 2 clickuri consecutive pe buton
	@Test
	public void testDoubleClickPowerButtonStartTrackingOtherConditionsAcomplished() {
		final SharedPreferences shared = tripMainActivity.getSharedPreferences("com.example.prefs", Context.MODE_PRIVATE);
		final boolean initial = shared.getBoolean("tracking", false);
		final ImageButton powerButton = (ImageButton) tripMainActivity.findViewById(R.id.btnStartTrip);
		//pt simulare click pe buton
		TouchUtils.clickView(this, powerButton);
		//astept putin pana a da click din nou petru a vedea daca si-a schimbat starea
		new Handler().postDelayed(new Runnable() {
			
			@Override
			public void run() {
				TouchUtils.clickView(TripControlMainActivityTestCase.this, powerButton);
				boolean actual = shared.getBoolean("tracking", false);
				assertEquals("erroare, nu poate fi in starea tracking dupa 2 click-uri consecutive", initial, actual);
			}
		}, 30000); //astept 30 secunde
	}
	
	public void testLayout() {
	    int tvNewTrip = R.id.tvNewTrip;
	    assertNotNull(tripMainActivity.findViewById(tvNewTrip));
	    TextView view = (TextView) tripMainActivity.findViewById(tvNewTrip);
	    assertEquals("Label Incorrect", "New Trip", view.getText());
	  }
}
