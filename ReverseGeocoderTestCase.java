package com.example.testcase;

import junit.framework.TestCase;

import org.junit.Test;

import android.util.Log;

import com.example.rest.services.ReverseGeocoderAsync;

public class ReverseGeocoderTestCase extends TestCase {
	
	@Test
	public void testNullInput() {
		new ReverseGeocoderAsync() {
			protected void onPostExecute(String result) {
				assertTrue("nu verifica inputul primit", result == null);
			};
		}.execute(null, null);
	}
	
	@Test
	public void testResultAddress() {
		new ReverseGeocoderAsync() {
			@Override
			protected void onPostExecute(String result) {
				String actual = result;
				String expected = "Strada Mihail Moxa 17, Bucuresti, Romania";
				assertEquals("nu proceseaza corect", actual, expected);
			};
		}.execute(44.446434020996094, 26.086803436279297);
	}

}
